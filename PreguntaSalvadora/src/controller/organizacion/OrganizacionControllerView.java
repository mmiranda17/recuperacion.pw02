package controller.organizacion;

import java.io.IOException;

import javax.jdo.PersistenceManager;
import javax.servlet.RequestDispatcher;

import model.entity.PMF;
import model.entity.Organizacion;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.*;

import model.entity.Organizacion;
import model.entity.PMF;

@SuppressWarnings("serial")
public class OrganizacionControllerView extends HttpServlet{

	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		resp.setContentType("text/plain");
		
		String id = req.getParameter("id");
		
		final PersistenceManager pm = PMF.get().getPersistenceManager();
		Organizacion u = pm.getObjectById(Organizacion.class, Long.parseLong(id));
		
		System.out.println("id: "+id);
		
		String message = null;

		try{
			req.setAttribute("organizacion", u);
			RequestDispatcher rd = req.getRequestDispatcher("/Views/Organizacion/view.jsp");
			rd.forward(req, resp);
		}catch(Exception e){
			resp.getWriter().println(e);
		}finally{
			pm.close();
		}
	}
	
}
