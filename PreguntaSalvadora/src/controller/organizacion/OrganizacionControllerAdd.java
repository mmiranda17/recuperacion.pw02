package controller.organizacion;

import java.io.IOException;

import javax.jdo.PersistenceManager;
import javax.servlet.http.*;

import model.entity.Organizacion;
import model.entity.PMF;

@SuppressWarnings("serial")
public class OrganizacionControllerAdd extends HttpServlet{

	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		resp.setContentType("text/plain");
		

		String name = req.getParameter("name");
		String contrasena = req.getParameter("contrasena");
		String email = req.getParameter("email");
		
		Organizacion nuevo = null;
		final PersistenceManager pm = PMF.get().getPersistenceManager();
		nuevo = new Organizacion(name, contrasena, email);
		try{
			pm.makePersistent(nuevo);
			//System.out.println("aa: "+pm.makePersistent(p));
			resp.getWriter().println("Datos grabados correctamente.");
			resp.sendRedirect("/InicioSesion.html");
		}catch(Exception e){
			System.out.println(e);
			resp.getWriter().println("Ocurri� un error, vuelva a intentarlo.");
			resp.sendRedirect("/index.jsp");
		}finally{
			pm.close();
		}
	}
}
