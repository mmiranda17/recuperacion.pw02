package controller.organizacion;

import java.io.IOException;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.entity.Organizacion;
import model.entity.PMF;



@SuppressWarnings("serial")
public class OrganizacionControllerIndex extends HttpServlet{
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		//resp.setContentType("text/plain");
		
		final PersistenceManager pm = PMF.get().getPersistenceManager();
		String query = "select from " + Organizacion.class.getName();
		final Query q = pm.newQuery(query);
		
		
					 
			try{
				@SuppressWarnings("unchecked")
				List<Organizacion> organizaciones = (List<Organizacion>) q.execute();
				System.out.println("tam: "+ organizaciones.size());
				req.setAttribute("organizaciones", organizaciones);
				RequestDispatcher rd = getServletContext().getRequestDispatcher("/Views/Organizacion/index.jsp");
				rd.forward(req, resp);
			}catch(Exception e){
				System.out.println(e);
			}finally{
				q.closeAll();
				pm.close();
			}
		}			
	
}
