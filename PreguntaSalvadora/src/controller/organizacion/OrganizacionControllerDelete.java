package controller.organizacion;

import java.io.IOException;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.entity.*;

@SuppressWarnings("serial")
public class OrganizacionControllerDelete extends HttpServlet{

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		resp.setContentType("text/plain");		
	
				
				String id = req.getParameter("id");
				//System.out.println("id: "+id);
				PersistenceManager pm = PMF.get().getPersistenceManager();
				 //Escoje toda la clase usuario
				Organizacion c = pm.getObjectById(Organizacion.class, Long.parseLong(id) );

	
				try{
					//q.deletePersistentAll(id);
					pm.deletePersistent(c);
					
				
					resp.getWriter().println("Se han borrado organizacion.");
					resp.sendRedirect("/index");
				}catch(Exception e){
						System.out.println(e);
						resp.getWriter().println("No se han podido borrar organizaciones.");
						resp.sendRedirect("/index.jsp");
				}finally{
					//q.closeAll();
					pm.close();
				}		
		
	}
}
