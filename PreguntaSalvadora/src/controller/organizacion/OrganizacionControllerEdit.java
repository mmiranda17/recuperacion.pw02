package controller.organizacion;

import java.io.IOException;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.*;

import model.entity.Organizacion;
import model.entity.PMF;

@SuppressWarnings("serial")
public class OrganizacionControllerEdit extends HttpServlet{
	
	
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		resp.setContentType("text/plain");
		
		String id = req.getParameter("id");
		
		final PersistenceManager pm = PMF.get().getPersistenceManager();
		Organizacion u = pm.getObjectById(Organizacion.class, Long.parseLong(id));
		
		String message = null;

		try{
			req.setAttribute("organizacion", u);
			RequestDispatcher rd = req.getRequestDispatcher("/Views/Organizacion/edit.jsp");
			rd.forward(req, resp);
		}catch(Exception e){
			resp.getWriter().println(e);
		}finally{
			pm.close();
		}
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		resp.setContentType("text/plain");
		
		String id = req.getParameter("id");
		String 	email = req.getParameter("email");
		String 	name = req.getParameter("name");
		String 	contrasena = req.getParameter("contrasena");
		
		System.out.println("id: "+id);
		
		final PersistenceManager pm = PMF.get().getPersistenceManager();
		
	    try {
	    	Organizacion u = pm.getObjectById(Organizacion.class, Long.parseLong(id));
	    	u.setName(name);
			u.setContrasena(contrasena);
			u.setEmail(email);
			resp.sendRedirect("/index");	
	    } finally {
	        pm.close();
	    }
	
	}
}
