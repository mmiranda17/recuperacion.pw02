package controller.producto;


import java.io.IOException;

import javax.jdo.PersistenceManager;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.*;

import com.google.appengine.api.users.UserServiceFactory;

import model.entity.Organizacion;
import model.entity.PMF;
import model.entity.Producto;

@SuppressWarnings("serial")
public class ProductoControllerAdd extends HttpServlet{
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException{
		
		com.google.appengine.api.users.User uGoogle = UserServiceFactory.getUserService().getCurrentUser();
		
		String nombre = req.getParameter("nombre");
		double precio = Double.parseDouble(req.getParameter("precio"));
		String descripcion = req.getParameter("descripcion");
		String pertenencia = uGoogle.getEmail();
		
		
		String message = null;
		
		Producto nuevo = null;
		final PersistenceManager pm = PMF.get().getPersistenceManager();
		nuevo = new Producto(nombre, precio, descripcion, pertenencia);		
		try{
			pm.makePersistent(nuevo);

			resp.getWriter().println("Datos grabados correctamente.");
			resp.sendRedirect("/ver_productos");
			System.out.println(pertenencia);
		}catch(Exception e){
			System.out.println(e);
			message="Ocurrio un proble al crear el producto";
		}finally{
			pm.close();
		}
					
	}	
}