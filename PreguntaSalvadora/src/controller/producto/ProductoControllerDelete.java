package controller.producto;

import java.io.IOException;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.*;

import model.entity.Producto;
import model.entity.PMF;

@SuppressWarnings("serial")
public class ProductoControllerDelete extends HttpServlet {
	
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
		throws IOException{
		resp.setContentType("text/plain");
		
		Long id = Long.parseLong(req.getParameter("id"));
		
		final PersistenceManager pm = PMF.get().getPersistenceManager();
		Producto p = pm.getObjectById(Producto.class, id);
		
		String message = null;
		try{
			pm.deletePersistent(p);
			message = "Producto Eliminado";
			resp.sendRedirect("/ver_productos");
		}catch(Exception e){
			message = "No se pudo eliminar el producto";
		}finally{
			pm.close();
		}
	}
}