package controller.producto;

import java.io.IOException;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.*;

import com.google.appengine.api.users.UserServiceFactory;

import model.entity.Producto;
import model.entity.Organizacion;
import model.entity.PMF;

@SuppressWarnings("serial")
public class ProductoControllerEdit extends HttpServlet{
	
	
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		
		
		String id = req.getParameter("id");
		
		final PersistenceManager pm = PMF.get().getPersistenceManager();
		Producto p = pm.getObjectById(Producto.class, Long.parseLong(id));
		
		String message = null;

		try{
			req.setAttribute("producto", p);
			RequestDispatcher rd = req.getRequestDispatcher("/Views/Producto/edit.jsp");
			rd.forward(req, resp);
		}catch(Exception e){
			resp.getWriter().println(e);
		}finally{
			pm.close();
		}
	}
	
	public void  doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException{
		
		com.google.appengine.api.users.User uGoogle = UserServiceFactory.getUserService().getCurrentUser();
		
		Long id = Long.parseLong(req.getParameter("id"));
		String nombre = req.getParameter("nombre");
		String descripcion = req.getParameter("descripcion");
		double precio = Double.parseDouble(req.getParameter("precio"));
		String pertenencia = uGoogle.getEmail();
		
		final PersistenceManager pm = PMF.get().getPersistenceManager();
		String message = "Hola Mundo XD";
		Producto p = null;
		
		
		
		try{
			p = pm.getObjectById(Producto.class, id);
			p.setNombre(nombre);
			p.setDescripcion(descripcion);
			p.setPrecio(precio);
			p.setPertenencia(pertenencia);
		}catch(Exception e){
			message = "Ocurrio un error al actualizar el producto";
		}
		
		try{
			pm.makePersistent(p);
			message="Se actualizo con exito el producto";
		}catch(Exception e){
			message = "Ocurrio un error al guardar los datos";
		}finally{
			pm.close();
		}
		
		resp.sendRedirect("/ver_productos");
	}
}