<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="model.entity.Producto"%>
<%@ page import="java.util.List"%>

<%@ page import="com.google.appengine.api.users.*;"%>

<% List<Producto> productos = (List<Producto>)request.getAttribute("productos");%>
<% com.google.appengine.api.users.User uGoogle = UserServiceFactory.getUserService().getCurrentUser(); %>
<% String pertenencia = uGoogle.getEmail(); %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html lang="es">
<head>
<title>Productos CRUD</title>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" 
	integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>
<!-- Menú de navegación del sitio -->
<ul class="navbar">
  <li><a href="/index.html">Regresar...</a>
  
</ul>
	<div class="top-bar-section">
  			<ul class="right">
  				<li><a href="#">Inicio</a></li>
  				<li><a href="/Views/Comprobante/index.jsp">Comprobantes</a></li>
  				<li><a href="/index">Usuarios</a></li>
  				<li><a href="/ver_productos">Productos</a></li>
  				<li><a href="/user/logout">Cerrar Sesion</a></li>
  			</ul>
  		</div>


	<h2>Lista Productos</h2>
	<a href="/Views/Producto/add.jsp"> Crear Producto</a>
	<table border="1" class="table table-bordered">
		<thead>
			<tr>
				<td align="center">Codigo</td>
				<td align="center">Nombre</td>				
				<td align="center">Precio</td>
				<td align="center">Descripcion</td>
				
				<td align="center">Action</td>
			</tr>
		</thead>
		
		<%
			
		if(request.getAttribute("productos")!=null){
				
			
				
			if(!productos.isEmpty()){
				 for(Producto u : productos){
					 if( pertenencia.equals(u.getPertenencia()) ){
						 
		%>
				<tr>
				  <td align="center"><%=u.getId() %></td>
				  <td align="center"><%=u.getNombre() %></td>
				  <td align="center"><%=u.getPrecio() %></td>
				  <td align="center"><%=u.getDescripcion() %></td>
				  
				  
				  <td align="center" >
				  	  
					  <a href="/producto/edit?id=<%=u.getId() %>"> Edit</a> |
                      <a href="/producto/delete?id=<%=u.getId() %>"> Delete</a>
                  </td>
				 
				</tr>
		<%	
					 }
				}
		    
			}
			
		   }
		%>
         
        </tr>
     
	</table>
</body>
</html>