<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="model.entity.Producto"%>
<%@ page import="java.util.List"%>

<%@ page import="com.google.appengine.api.users.*;"%>

<% List<Producto> productos = (List<Producto>)request.getAttribute("productos");%>
<% com.google.appengine.api.users.User uGoogle = UserServiceFactory.getUserService().getCurrentUser(); %>
<% String pertenencia = uGoogle.getEmail(); %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html lang="es">
<head>
<title>prueba comprobante</title>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" 
	integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>
<!-- Menú de navegación del sitio -->
<ul class="navbar">
  <li><a href="/index.html">Regresar...</a>
  
</ul>
	<div class="top-bar-section">
  			<ul class="right">
  				<li><a href="#">Inicio</a></li>
  				<li><a href="/Views/Comprobante/index.jsp">Comprobantes</a></li>
  				<li><a href="/index">Usuarios</a></li>
  				<li><a href="/ver_productos">Productos</a></li>
  				<li><a href="/user/logout">Cerrar Sesion</a></li>
  			</ul>
  		</div>


	 <h2>Factura</h1>
	  <div>
	  	<h1>Nro RUC: </h1>
	  	<h1>Razon Social: </h1>
	  </div>
	  
	 <a>Agregar Producto</a> 
	 <br>
	 
	<%
			
		if(request.getAttribute("productos")!=null){
	
			if(!productos.isEmpty()){
				 for(Producto u : productos){
					 if( pertenencia.equals(u.getPertenencia()) ){
						
		%>
	  
		  <a> <%=u.getNombre() %> <a>
		  <br>
	
		<br>
	<table border="1" class="table table-bordered">
		<thead>
			<tr>
				<td align="center">Codigo</td>
				<td align="center">Descripcion</td>	
				<td align="center">Cantidad</td>			
				<td align="center">Precio</td>
				<td align="center">Importe</td>
	
				<td align="center">Action</td>
			</tr>
		</thead>
     		<tr>
				  <td align="center"><%=u.getId() %></td>
				  <td align="center"><%=u.getNombre() %></td>
				  
				  <td align="center">
				  	<input type="text" name="cantidadIn" maxlength="3" required >
				  </td>
				  <% int cantidad = 2; %>
				  <td align="center"><%=u.getPrecio() %></td>
				  <td align="center"><%=u.getPrecio()*cantidad %></td>
				  
				  
				  <td align="center" >
				  	  <a href="#"> +</a> |
                      <a href="#"> -</a>
                  </td>
				
			</tr>
				<%	
					 }
				}
		    
			}
			
		   }
		%>
	</table>
</body>
</html>