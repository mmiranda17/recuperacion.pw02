<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<html lang="es">
<%@ page import="model.entity.Organizacion"%>

<% Organizacion user = (Organizacion)request.getAttribute("organizacion");%>
<head>
<title>Update Organizacion </title>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" 
	integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	
</head>
<body>
<!-- Menú de navegación del sitio -->
<ul class="navbar">
  <li><a href="/index">Regresar...</a>
</ul>

<form class="form-horizontal col-md-6" action="edit?id=<%=user.getId() %>" method="post">
<fieldset><legend>Actualizar Organizacion</legend>
	 <div class="form-group">
	    <label for="name" class="col-md-2 control-label" >Nombre: </label>
	    <div class="col-md-6">
	    	<input type="text" class="form-control" name="name" maxlength="20" value="<%=user.getName() %>" required>
	    </div>
	  </div>
	   <div class="form-group">
	    <label for="name" class="col-md-2 control-label">Contrasena: </label>
	    <div class="col-md-6">
	    	<input type="text" class="form-control" name="contrasena" maxlength="20" value="<%=user.getContrasena() %>" required>
	    </div>
	    
	  </div>
	   <div class="form-group">
	    <label for="name" class="col-md-2 control-label">Correo: </label>
	    <div class="col-md-6">
	    	<input type="text" class="form-control" name="email" maxlength="50" value="<%=user.getEmail() %>" required>
	    </div>
	    
	  </div>

	
	<div class="form-group">
		<div id="action">
			<input class="btn btn-default" type="reset" value="Limpiar"/>
			<input class="btn btn-default" type="submit" value="Guardar"/>
		</div>
	</div>

</fieldset>
</form>

</body>
</html>