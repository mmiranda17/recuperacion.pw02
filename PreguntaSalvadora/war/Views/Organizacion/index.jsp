<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="model.entity.Organizacion"%>
<%@ page import="java.util.List"%>
<% List<Organizacion> organizaciones = (List<Organizacion>)request.getAttribute("organizaciones");%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html lang="es">
<head>
<title>Organizacion CRUD</title>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" 
	integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>
<!-- Menú de navegación del sitio -->
<ul class="navbar">
  <li><a href="/index.html">Regresar...</a>
  <li><a href="/index.jsp">Registro</a>
</ul>



	<h2>Lista Organizaciones</h2>
	<table border="1" class="table table-bordered">
		<thead>
			<tr>
				<td align="center">Codigo</td>
				<td align="center">Organizacion</td>				
				<td align="center">Correo Electronico</td>
				<td align="center">Contraseña</td>
				
				<td align="center">Action</td>
			</tr>
		</thead>
		
		<%
			
		if(request.getAttribute("organizaciones")!=null){
				
			List<Organizacion> productos = 
                           (List<Organizacion>)request.getAttribute("organizaciones");
				
			if(!organizaciones.isEmpty()){
				 for(Organizacion u : organizaciones){
						 
		%>
				<tr>
				  <td align="center"><%=u.getId() %></td>
				  <td align="center"><%=u.getName() %></td>
				  <td align="center"><%=u.getEmail() %></td>
				  <td align="center"><%=u.getContrasena() %></td>
				  
				  
				  <td align="center" >
				  	  <a href="/view?id=<%=u.getId() %>"> View</a> | 
					  <a href="/edit?id=<%=u.getId() %>"> Edit</a> | 
                      <a href="/delete?id=<%=u.getId() %>"> Delete</a>
                  </td>
				 
				</tr>
		<%	
			
				}
		    
			}
			
		   }
		%>
         
        </tr>
     
	</table>
</body>
</html>