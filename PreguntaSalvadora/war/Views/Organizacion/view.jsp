<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<html lang="es">
<%@ page import="model.entity.Organizacion"%>

<% Organizacion user = (Organizacion)request.getAttribute("usuario");%>
<head>
<title>Ver Usuario </title>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" 
	integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	
</head>
<body>
<!-- Menú de navegación del sitio -->
<ul class="navbar">
  <li><a href="/getList">Regresar...</a>
</ul>

<form class="form-horizontal col-md-6" action="view" method="get">
<fieldset><legend>Ver Usuario</legend>
	 <div class="form-group">
	    <label for="name" class="col-md-2 control-label" >Organizacion: </label>
	    <label for="name" class="col-md-2 control-label" ><%=user.getName() %> </label>
	  </div>
	   <div class="form-group">
	    <label for="name" class="col-md-2 control-label">Contrasena: </label>
	    <label for="name" class="col-md-2 control-label"><%=user.getContrasena() %> </label>
	  </div>
	   <div class="form-group">
	    <label for="name" class="col-md-2 control-label">Correo: </label>
	    <label for="name" class="col-md-2 control-label"><%=user.getEmail() %> </label>
	  </div>

	
	

</fieldset>
</form>

</body>
</html>